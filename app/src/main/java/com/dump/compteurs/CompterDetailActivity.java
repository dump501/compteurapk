package com.dump.compteurs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.dump.compteurs.adapters.DetailCompterAdapter;
import com.dump.compteurs.dao.IndexDao;

import java.util.ArrayList;

public class CompterDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compter_detail);

        //get the compter name using the intent extras
        Intent i = getIntent();

        if(i != null)
        {
            Bundle bundle = getIntent().getExtras();
            String compterName = bundle.getString("compterName");

            //set the compter name in the view
            TextView textView = findViewById(R.id.compter_detail_name);
            textView.setText(compterName);

            //get all indexes of the compter in an arraylist
            IndexDao indexDao = new IndexDao(this);

            ArrayList<String> indexArray = indexDao.getAllIndex(compterName);

            //set the first date and the first index
            TextView tv_date = findViewById(R.id.detail_date);
            TextView tv_index = findViewById(R.id.detail_index);

            tv_date.setText("date de creation: " + indexArray.get(2));
            tv_index.setText("premier index: " + indexArray.get(1));

            //delete the first 3 row of the array
            indexArray.set(0, "Index");
            indexArray.set(1, "Prix");
            indexArray.set(2, "date");

            //get the adapter
            DetailCompterAdapter adapter = new DetailCompterAdapter(this, indexArray);

            //select the grid view and set the adapter to populate
            GridView gridView = findViewById(R.id.compter_detail_gridview);
            gridView.setAdapter(adapter);
        }
    }
}
