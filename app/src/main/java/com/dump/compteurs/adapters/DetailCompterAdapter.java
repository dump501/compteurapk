package com.dump.compteurs.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dump.compteurs.R;

import java.util.ArrayList;

public class DetailCompterAdapter extends BaseAdapter {
    //attributs
    Context context;
    ArrayList<String> compterDetails;
    LayoutInflater inflater;

    public DetailCompterAdapter(Context context, ArrayList<String> compterDetails)
    {
        this.context = context;
        this.compterDetails = compterDetails;
        this.inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return compterDetails.size();
    }

    @Override
    public String getItem(int position) {
        return compterDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //inflate the layout
        convertView = inflater.inflate(R.layout.detail_compter_item, null);

        TextView textView = convertView.findViewById(R.id.tv_detail);
        textView.setText(compterDetails.get(position));

        return convertView;
    }
}
