package com.dump.compteurs.dao;

public class Compter {

    //attributs
    private String name;
    private int index;

    public Compter(String name, int index)
    {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
