package com.dump.compteurs.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CompterDao {

    //attributs
    SQLiteDatabase database;
    MySQLiteHelper dbHelper;

    public CompterDao(Context context)
    {
        dbHelper = new MySQLiteHelper(context);
    }

    //methods
    public void open()
    {
        database = dbHelper.getWritableDatabase();
    }

    public void close()
    {
        dbHelper.close();
    }

    public boolean create(String name, int index)
    {
        try
        {
            //open the database
            open();

            //store the data compter
            ContentValues values = new ContentValues();
            values.put(dbHelper.COLUMN_NAME, name);
            database.insert(dbHelper.TABLE_COMPTERS, null, values);


            //take the date and format it
            Date now = new Date();

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY");

            String creationDate = formatter.format(now);

            //store the index
            ContentValues valu = new ContentValues();
            valu.put(dbHelper.COLUMN_COMPTER_NAME, name);
            valu.put(dbHelper.COLUMN_INDEXE, index);
            valu.put(dbHelper.COLUMN_CREATION_DATE, creationDate);

            database.insert(dbHelper.TABLE_INDEXES, null, valu);

            close();
            //return the status
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public ArrayList<String> getAllCompter()
    {

        try
        {
            open();

            //query to select all compter names
            String req = "SELECT * from " + dbHelper.TABLE_COMPTERS;
            Cursor cursor = database.rawQuery(req, null);

            cursor.moveToFirst();
            //the array list to store the compters name
            ArrayList<String> compters = new ArrayList<>();

            //populate the array to return with the compter names
            while (cursor.moveToNext())
            {
                compters.add(cursor.getString(1));
            }

            close();
            //return the arraylist
            return compters;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
