package com.dump.compteurs.dao;

public class Index {

    //attributs
    String compterName;
    int index;
    String creationDate;

    //constructor
    public Index(String compterName, int index, String creationDate)
    {
        this.compterName = compterName;
        this.index = index;
        this.creationDate = creationDate;
    }

    //methods

    public int getIndex() {
        return index;
    }

    public String getCompterName() {
        return compterName;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setCompterName(String compterName) {
        this.compterName = compterName;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

}
