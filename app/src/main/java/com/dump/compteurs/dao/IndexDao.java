package com.dump.compteurs.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class IndexDao {

    //attibuts
    MySQLiteHelper dbHelper;
    SQLiteDatabase database;

    //contructor
    public IndexDao(Context context)
    {
        dbHelper = new MySQLiteHelper(context);
    }

    //methods
    public void open()
    {
        database = dbHelper.getWritableDatabase();
    }

    public void close()
    {
        dbHelper.close();
    }

    public boolean create(String compterName, int index)
    {

        try
        {
            //open the database
            open();

            /*String req = "SELECT * FROM " +dbHelper.TABLE_INDEXES;

            Cursor cursor = database.rawQuery(req, null);
            cursor.moveToFirst();

            if (cursor.)*/

            //take the date and format it
            Date now = new Date();

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY");

            String creationDate = formatter.format(now);

            //insert the data
            ContentValues values = new ContentValues();
            values.put(dbHelper.COLUMN_COMPTER_NAME, compterName);
            values.put(dbHelper.COLUMN_INDEXE, index);
            values.put(dbHelper.COLUMN_CREATION_DATE, creationDate);

            database.insert(dbHelper.TABLE_INDEXES, null, values);

            //close database
            close();

            //return the status
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public ArrayList<String> getAllIndex(String compterName)
    {
        try
        {
            //open the database
            open();

            //select the indexs and the dates of the compter
            String req = "SELECT * FROM " +dbHelper.TABLE_INDEXES + " WHERE " + dbHelper.COLUMN_COMPTER_NAME + " = ?";

            Cursor cursor = database.rawQuery(req, new String[]{compterName});

            ArrayList<String> indexArray = new ArrayList<>();

            cursor.moveToFirst();

            //populate the array
            indexArray.add(cursor.getString(1));
            indexArray.add(String.valueOf(cursor.getInt(2)));
            indexArray.add(cursor.getString(3));

            while (cursor.moveToNext())
            {
                //add the new index to the array
                indexArray.add(String.valueOf(cursor.getInt(2)));

                //new index
                int newIndex = cursor.getInt(2);

                //old index
                cursor.moveToPrevious();
                int oldIndex = cursor.getInt(2);
                cursor.moveToNext();

                String priceColumn="index mal relever";
                //price
                int price = (newIndex - oldIndex)*50;

                if(price <0)
                {
                    priceColumn = "index mal relevé";
                }
                else
                {
                    priceColumn = String.valueOf(price).concat(" FCFA");
                }

                //add to the array
                indexArray.add(priceColumn);

                //add the date to the array
                indexArray.add(cursor.getString(3));
            }

            //close the database
            close();

            //return the arraylist of index
            return indexArray;

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
