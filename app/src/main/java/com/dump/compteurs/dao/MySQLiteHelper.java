package com.dump.compteurs.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

    //attributs

    //table compters
    public static final String TABLE_COMPTERS = "compters";
    public static final String COLUMN_ID_COMPTER  = "id_compter";
    public static final String COLUMN_NAME = "name";

    //table indexes
    public static final String TABLE_INDEXES = "indexes";
    public static final String COLUMN_ID_INDEXE = "id_indexes";
    public static final String COLUMN_COMPTER_NAME = "compter_name";
    public static final String COLUMN_INDEXE = "indexs";
    public static final String COLUMN_CREATION_DATE = "creation_date";

    //database
    private static final String DATABASE_NAME = "index_saver_db";
    private static final int DATABASE_VERSION = 1;

    //string to create database
    public static final String CREATE_TABLE_COMPTERS = "create table " +
            TABLE_COMPTERS + " (" + COLUMN_ID_COMPTER  + " integer primary key autoincrement, " + COLUMN_NAME + " text not null );";

    public static final String CREATE_TABLE_INDEXES = "create table " +
            TABLE_INDEXES + " (" + COLUMN_ID_INDEXE + " integer primary key autoincrement, " + COLUMN_COMPTER_NAME + " text not null, " + COLUMN_INDEXE + " integer not null, " + COLUMN_CREATION_DATE + " text not null);";


    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //methods
    @Override
    public void onCreate(SQLiteDatabase database) {

        try
        {

            database.execSQL(CREATE_TABLE_COMPTERS);
            database.execSQL(CREATE_TABLE_INDEXES);

            ContentValues values = new ContentValues();

            values.put(MySQLiteHelper.COLUMN_NAME, "kkkkkkkkk");


            database.insert(MySQLiteHelper.TABLE_COMPTERS, null, values);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.w(MySQLiteHelper.class.getName(),
                "Updating database from version "+ oldVersion + " to " +
                        newVersion +", which will destroy the old data");

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPTERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INDEXES);
        onCreate(db);

    }

}
