package com.dump.compteurs.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.dump.compteurs.R;
import com.dump.compteurs.dao.CompterDao;
import com.dump.compteurs.ui.main.PageViewModel;

import java.util.ArrayList;

public class CompterListFragment extends Fragment {

    //attributs
    PageViewModel pageViewModel;
    CompterDao compterDao;
    ArrayList<String> compterList;
    ArrayAdapter adapter;

    //constructors
    public CompterListFragment()
    {

    }

    //methods

    public static Fragment newInstance()
    {
        return new CompterListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //select data from database and put in an array
        compterDao = new CompterDao(getContext());

        compterList = compterDao.getAllCompter();

        //create the array adapter
        adapter = new ArrayAdapter<>(getContext(), R.layout.compter_item, compterList);


        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        //create the view to return
        View root = inflater.inflate(R.layout.fragment_compter_list, container, false);

        //instanciate the liste view and bind the
        ListView listView = root.findViewById(R.id.compter_list);
        listView.setAdapter(adapter);


        //return the view
        return root;
    }

}
