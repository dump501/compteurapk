package com.dump.compteurs.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.dump.compteurs.MainActivity;
import com.dump.compteurs.R;
import com.dump.compteurs.dao.CompterDao;
import com.dump.compteurs.dao.IndexDao;
import com.dump.compteurs.ui.main.PageViewModel;

import java.util.ArrayList;

public class IndexRegFragment extends Fragment {

    //attributs
    PageViewModel pageViewModel;
    CompterDao compterDao;
    ArrayList<String> compterList;
    ArrayAdapter adapter;

    //constructor
    public IndexRegFragment()
    {

    }


    public static Fragment newInstance()
    {
        return new IndexRegFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //select data from database
        compterDao = new CompterDao(getContext());
        compterList = compterDao.getAllCompter();

        //instanciate the adapter
        adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, compterList);

        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //inflate the layout of this fragment
        View root = inflater.inflate(R.layout.fragment_index_reg, container, false);

        //get the spinner
        final Spinner spinner = root.findViewById(R.id.spinner_index_reg);

        //populate the spinner
        spinner.setAdapter(adapter);

        //get the register button
        Button regButton = root.findViewById(R.id.btn_compter_reg);

        final EditText index_text = root.findViewById(R.id.compter_index);

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get the data entered by the user
                String compterName = (String) spinner.getSelectedItem();
                int index = Integer.parseInt(index_text.getText().toString());

                //store in the database
                IndexDao indexDao = new IndexDao(getContext());

                indexDao.create(compterName, index);

                AlertDialog.Builder pop = new AlertDialog.Builder(getActivity());
                pop.setTitle(R.string.popup_success_index_reg);
                pop.setNeutralButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //recreate the activity
                        Intent i = new Intent(getContext(), MainActivity.class);
                        startActivity(i);
                    }
                });
                pop.show();

                index_text.setText("");

                //redirect to the compter list activity

            }
        });

        //return the view
        return root;

    }
}
