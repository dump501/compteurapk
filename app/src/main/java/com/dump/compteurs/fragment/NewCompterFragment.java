package com.dump.compteurs.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.dump.compteurs.MainActivity;
import com.dump.compteurs.R;
import com.dump.compteurs.dao.CompterDao;
import com.dump.compteurs.ui.main.PageViewModel;

public class NewCompterFragment extends Fragment {

    //attributs
    PageViewModel pageViewModel;
    CompterDao compterDao;

    //constructors
    public NewCompterFragment()
    {

    }

    public static Fragment newInstance() {

        return new NewCompterFragment();
    }

    //methods


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //instanciate the compter data access object
        compterDao = new CompterDao(getContext());

        //instanciate the page view model
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //create an inflate the view
        final View root = inflater.inflate(R.layout.fragment_new_compter, container, false);

        //when the register button is clicked
        Button btnReg = root.findViewById(R.id.btn_compter_register);
        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get the data intered by the user
                EditText compterEdit = root.findViewById(R.id.compter_reg_name);
                EditText indexEdit = root.findViewById(R.id.compter_reg_index);

                String compterName = compterEdit.getText().toString();
                String index = indexEdit.getText().toString();

                int compterIndex = 0;

                //verify the data
                if(compterName.equals("") || index.equals(""))
                {
                    AlertDialog.Builder pop = new AlertDialog.Builder(getActivity());
                    pop.setTitle(R.string.popup_error_title_reg);
                    pop.setNeutralButton("ok", null);
                    pop.show();
                }
                else
                {
                    compterIndex = Integer.parseInt(index);
                    //store in the database
                    CompterDao compterDao = new CompterDao(getContext());
                    compterDao.create(compterName, compterIndex);

                    AlertDialog.Builder pop = new AlertDialog.Builder(getActivity());
                    pop.setTitle(R.string.popup_success_compter_reg);
                    pop.setNeutralButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //recreate the activity
                            Intent i = new Intent(getContext(), MainActivity.class);
                            startActivity(i);
                        }
                    });
                    pop.show();

                    compterEdit.setText("");
                    indexEdit.setText("");

                }


            }
        });


        //return the view
        return root;

    }
}
